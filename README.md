# Mac Keyboard for Windows 10

This project aims to provide THE most accurate **Mac Keyboard Layout** for **Windows 10**, if you find any issues you can either post an issue here or contact me on Discord. For now only fr-fr, en-us, en-uk, de-ch, de-de templates are builded and supposed to be complete, feedback welcome !

## Download & Installation

https://gitlab.com/aar642/mac-keyboard/-/archive/master/mac-keyboard-master.zip

- Run **setup.exe** from build folder of any languages you want to install

## Maintainers
![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
[![BuyMeCoffee][buymecoffeebadge]][buymecoffee]

[buymecoffee]: https://www.buymeacoffee.com/latqazuzw
[buymecoffeebadge]: https://img.shields.io/badge/buy%20me%20a%20coffee-donate-yellow?style=for-the-badge


